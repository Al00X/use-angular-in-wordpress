# **Using Angular in Wordpress**
I have prepared all the resources and instructions you need to
implement angular into your Wordpress, and it's easy as a
piece of cake!

I assume you already set-up your Wordpress, so we go straight to the point.


### **First Step: Preparing Angular**
Create a project using Angular-CLI (`ng new`) and install all the packages
if Angular didn't do it for you (`npm i`).

We don't have to set any settings for angular, so just build the app
(`ng build`)

After that, create a `.php` file in your application root and name it anything
you like. Add the code below to this file.

<pre>&#60;?php
 /**
 * Plugin Name: *YOUR PLUGIN NAME*
 * Description: *YOUR PLUGIN DESC*
 */

 // This function sets reference to our built files in dist
 function func_load_angular() {
     wp_register_script('test_main', plugin_dir_url( __FILE__ ).'dist/main.js', true);
     wp_register_script('test_polyfills', plugin_dir_url( __FILE__ ).'dist/polyfills.js', true);
     wp_register_script('test_runtime', plugin_dir_url( __FILE__ ).'dist/runtime.js', true);
     wp_register_script('test_styles', plugin_dir_url( __FILE__ ).'dist/styles.js', true);
     wp_register_script('test_vendor', plugin_dir_url( __FILE__ ).'dist/vendor.js', true);
 }
 add_action('wp_load_scripts', 'func_load_angular');

// This function loads our scripts
 function func_load_plugin(){
     wp_load_script('test_main');
     wp_load_script('test_polyfills');
     wp_load_script('test_runtime');
     wp_load_script('test_styles');
     wp_load_script('test_vendor');

     // This function also returns a string for our shortcode
     <u>$str= '&#60;app-root&#62;&#60;/app-root&#62;';</u>
     return $str;
 }

 // And at last, add it to the shortcodes with the given name
  <u>add_shortcode( 'test', 'func_load_plugin' );</u>
?>
</pre>
<ul><li>The first underlined code is the html tag of our main component
in Angular which application loads into.</li>
<li>The second underlined code has two parameters. the first one is important
for us, because it's the name of the shortcode that we will use it later.</li></ul>

Our plugin is ready! now add the folder to `wp-content/plugins` and activate it.

### **Second Step: Preparing Wordpress**
We don't need our current theme, we need to use Angular fully for Front-End.
To do this we use a theme that I created already for this purpose [Theme](./theme).

I named it 'empty' theme, because it's literally empty. Copy this theme to
`wp-content/themes` and activate it.

We are not ready yet, we need to change some settings in our theme.

Open Theme-Editor (`Appearance > Theme-Editor`) in Wordpress.
Select the theme from the dropdown menu and choose `Main Index Template`
or `index.php`.

You need to change two lines:

#### First Line
<pre>&#60;base href="/angular/"></pre>
Change this to the root path of your website.
Most of the cases its `"/"` but I had to change it because I used **XAMPP**
to create my Wordpress.
#### Second Line
<pre>&#60;?php echo do_shortcode("[test]"); ?></pre>
And finally, change `test` to whatever you chose for your shortcode name.

<br><br>
Done! simple as that. Now you can edit your angular project, and build it when  
you where happy with it.
